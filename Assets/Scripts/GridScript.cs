﻿using UnityEngine;
using System.Collections;


public class GridScript : MonoBehaviour {

	private CellController[,] Grid;
	// CellController[,] cellControllers = new CellController[10,10];
	private int height;
	private int width;
	// Use this for initialization
	public GameObject cellTemplate;

	private Vector3 previousMousePosition;
	private Vector3 mouseDelta;

	private float cameraSpeed = 0.005f;
	private float cameraZoomScale = 0.8f;

	private Camera camera;

	public static bool gameOn = false;

	private float twoFingerDist = 0f;

    private Vector2 DragFingerOriginalPosition;

    public static bool GliderOn = false;

	public void toggleGame(){
		gameOn = !gameOn;
	}

    public const float GameStepSize = 0.1f;
    public volatile float SliderMultiplier = 1f;
    private volatile float NextTick = 0f;

	void Start () {

#if UNITY_IOS && Unity_Editor
        Debug.Log("Defined IOS");
#endif

        camera = GameObject.Find("Main Camera").GetComponent<Camera>();

		previousMousePosition = Input.mousePosition;

		height = 100;
		width = 100;

		Grid = new CellController[height, width];

		Vector3 spawnLocation = new Vector3(0.5f,0.5f,0.5f);
		for(int i = 0; i<height; i++){
			for(int j = 0; j<width; j++){
				GameObject cellClone =  (GameObject)Instantiate(cellTemplate, spawnLocation, transform.rotation);
				Grid[i, j] = cellClone.GetComponent<CellController>();
				Grid[i, j].i = i;
				Grid[i, j].j = j;
				spawnLocation.x += 1.0f;
			}
			spawnLocation.y += 1.0f;
			spawnLocation.x = 0.5f;
		}
	}

    public ref CellController getCellAt(int i, int j)
    {
        return ref Grid[i, j];
    }

    public void ChangeSliderValue(float NewValue)
    {
        SliderMultiplier = 1/NewValue;
        tick();
        Debug.Log("Slider value is " + NewValue);
    }

	public void KillAll(){
		foreach(CellController cellController in Grid){
			cellController.deactivate();
		}
	}

    public void Randomize()
    {
        Random.Range(0, 1);
        foreach (CellController cellController in Grid)
        {
            if (Random.value >= 0.5f) { 
                cellController.deactivate();
            }
            else
            {
                cellController.activate();
            }
        }
    }

    public void GliderToggle()
    {
        GliderOn = !GliderOn;
    }

    public void reCenterCamera() {
        camera.transform.position = new Vector3(0, 0, 0);
        camera.orthographicSize = 5;
    }

	void HandleOneFinger(){
		if (Input.touches.Length==1){ //Handle touches
			Touch t1 = Input.touches[0];


            if (t1.phase == TouchPhase.Began) {
                DragFingerOriginalPosition = t1.position;
            }
			
			if(t1.phase == TouchPhase.Moved){
				Vector3 delta = t1.deltaPosition;
				camera.transform.position = camera.transform.position - delta*0.01f*camera.orthographicSize;
			}

            Ray ray = Camera.main.ScreenPointToRay(t1.position);

            if (t1.phase == TouchPhase.Ended && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
                if (GridScript.gameOn)
                    GridScript.gameOn = false;

                Vector2 test = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                RaycastHit2D hit = Physics2D.Raycast(test, (Input.GetTouch(0).position));
                if (hit.collider && Vector2.Distance(Input.GetTouch(0).position, DragFingerOriginalPosition) < 10) {
                    if (!GliderOn)
                    {
                        Debug.Log(hit.collider.gameObject.name);
                        hit.collider.gameObject.GetComponent<CellController>().flip();
                    }
                    else
                    {
                        int cursorI = hit.collider.gameObject.GetComponent<CellController>().j;
                        int cursorJ = hit.collider.gameObject.GetComponent<CellController>().i;
                        DrawGlider(cursorI, cursorJ);
                    }
                }

            }
		}
	}

   public void DrawGlider(int i, int j)
    {
        Grid[j, i].activate();
        Grid[j, i + 1].activate();
        Grid[j, i + 2].activate();
        Grid[j + 1, i + 2].activate();
        Grid[j + 2, i + 1].activate();
    }

	void HandleTwoFinger(){
		if(Input.touches.Length ==2){
			Touch t1 = Input.touches[0];
			Touch t2 = Input.touches[1];
			float currentDistance = Mathf.Abs(Vector3.Magnitude(t1.position- t2.position));

			if(t2.phase == TouchPhase.Moved && t1.phase == TouchPhase.Moved){
				camera.orthographicSize = twoFingerDist/currentDistance*camera.orthographicSize;
			}

			twoFingerDist = currentDistance;

		}
	}

    #if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
    void PCControls()
    {
        if (Input.GetMouseButton(1))
        {
            mouseDelta = Input.mousePosition - previousMousePosition;
            camera.transform.position = camera.transform.position - camera.orthographicSize * mouseDelta * cameraSpeed;

            Debug.Log("Mouse pos = " + Input.mousePosition);
            Debug.Log("Mouse Delta = " + mouseDelta);
        }

        if (Input.GetAxisRaw("Mouse ScrollWheel")>0)
        {
            //1.5 47

            camera.orthographicSize = Mathf.Clamp(cameraZoomScale * camera.orthographicSize, 1.5f, 47f);
            Debug.Log("hit up");
        }

        if (Input.GetAxisRaw("Mouse ScrollWheel")<0)
        {
            camera.orthographicSize = Mathf.Clamp(1 / cameraZoomScale * camera.orthographicSize, 1.5f, 47f);
            Debug.Log("hit up");
        }

        previousMousePosition = Input.mousePosition;

    }
    #endif

    // Update is called once per frame
    void Update () {

		if(gameOn && Time.time > NextTick){
			tick ();
		}

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        PCControls();
        #endif

        HandleOneFinger();
		HandleTwoFinger();


	
	}

	void tick(){
		//foreach(CellController[] cellControlRow in cellControllers){
		foreach(CellController cellController in Grid){
				int numNeighbors = getNumAliveNeighbors(cellController.j, cellController.i);
				if(numNeighbors <2){
					//cellController.activateBlack();
					cellController.futureStateIsWhite =false;
				}
				else if(numNeighbors ==3){
					//cellController.activateWhite();
					cellController.futureStateIsWhite =true;
				}
				else if(numNeighbors == 2 && cellController.isAlive == 1){
					cellController.futureStateIsWhite = true;
				}
				else {
					//cellController.activateBlack();
					cellController.futureStateIsWhite =false;
				}
		}

		foreach(CellController cellController in Grid){
			int numNeighbors = getNumAliveNeighbors(cellController.j, cellController.i);
			if(cellController.futureStateIsWhite){
				cellController.activate();
			}
			else{
				cellController.deactivate();
			}

		}

        NextTick = Time.time + SliderMultiplier/5;
	}

	public int getNumAliveNeighbors(int j, int i) {
		if (j == 0) {
			// bottom left corner
			if (i == 0) {
				return Grid[i,j+1].isAlive + Grid[i+1,j].isAlive + Grid[i+1,j+1].isAlive;
			}
			// top left corner
			else if (i == height-1) {
				//Debug.Log ("top left");
				//Debug.Log ("below is " + cellControllers[xidx,yidx-1].isAlive);
				return Grid[i,j+1].isAlive + Grid[i-1,j].isAlive + Grid[i-1,j+1].isAlive;
			}
			// left edge
			else {
				return Grid[i-1,j].isAlive + Grid[i+1,j].isAlive +
					Grid[i-1,j+1].isAlive + Grid[i,j+1].isAlive + Grid[i+1,j+1].isAlive;
			}
		} 
		else if (j == width-1) {
			// bottom right
			if (i == 0) {
				return Grid[i,j-1].isAlive + Grid[i+1,j-1].isAlive + Grid[i+1,j].isAlive;
			}
			// top right
			else if (i == height-1) {
				return Grid[i,j-1].isAlive + Grid[i-1,j-1].isAlive + Grid[i-1,j].isAlive;
			}
			// right edge
			else {
				return Grid[i+1,j].isAlive + Grid[i+1,j-1].isAlive + Grid[i,j-1].isAlive +
					Grid[i-1,j-1].isAlive + Grid[i-1,j].isAlive;
			}
		}
		// bottom edge
		else if (i == 0) {
			return Grid[i,j+1].isAlive + Grid[i+1,j+1].isAlive + Grid[i+1,j].isAlive + 
				Grid[i+1,j-1].isAlive + Grid[i,j-1].isAlive;
		}
		// top edge
		else if(i == height-1) {
			return Grid[i,j-1].isAlive + Grid[i-1,j-1].isAlive + Grid[i-1,j].isAlive +
				Grid[i-1,j+1].isAlive + Grid[i,j+1].isAlive;
		}
		// middle
		else {
			return Grid[i+1,j].isAlive + Grid[i+1,j-1].isAlive + Grid[i,j-1].isAlive +
				Grid[i-1,j-1].isAlive + Grid[i-1,j].isAlive + Grid[i-1,j+1].isAlive +
					Grid[i,j+1].isAlive + Grid[i+1,j+1].isAlive;
		}
	}

}
