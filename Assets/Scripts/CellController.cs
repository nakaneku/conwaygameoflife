﻿using UnityEngine;
using System.Collections;

public class CellController : MonoBehaviour {

	public GameObject blackSprite;
	public GameObject whiteSprite;
    public GameObject stagingSprite;

	public GridScript gridController;

	public int isAlive; //White is alive
	public int j; // x index in the grid
	public int i; // y index in the grid

	public bool futureStateIsWhite;

	// Use this for initialization
	void Start () {
		//Debug.Log("hi");
		isAlive = 0;
		gridController = GameObject.Find("Grid").GetComponent<GridScript>();
        whiteSprite.SetActive(false);
        stagingSprite.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	}

    private void OnMouseOver()
    {
        stagingSprite.SetActive(true);
        /*
         *         
        Grid[j, i].activate();
        Grid[j, i + 1].activate();
        Grid[j, i + 2].activate();
        Grid[j + 1, i + 2].activate();
        Grid[j + 2, i + 1].activate();
         * 
         * */
        if (GridScript.GliderOn)
        {
            gridController.getCellAt(i, j + 1).stage();
            gridController.getCellAt(i, j + 2).stage();
            gridController.getCellAt(i + 1, j + 2).stage();
            gridController.getCellAt(i +2, j + 1).stage();
        }
    }

    private void OnMouseExit()
    {
        stagingSprite.SetActive(false);
        if (GridScript.GliderOn)
        {
            gridController.getCellAt(i, j + 1).unstage();
            gridController.getCellAt(i, j + 2).unstage();
            gridController.getCellAt(i + 1, j + 2).unstage();
            gridController.getCellAt(i + 2, j + 1).unstage();
        }
    }

    //comment out to fix double activation bug

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
    void OnMouseDown()
    {



        if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            if (GridScript.gameOn)
                GridScript.gameOn = false;
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !GridScript.GliderOn)
            {
                flip();
            }
            else
            {
                gridController.DrawGlider(j, i);
            }
        }


    }

    #endif



    public void flip() {
        if (isAlive == 1) {
            deactivate();
        } else {
            activate();
        }
    }
	

	public void deactivate(){
        whiteSprite.SetActive(false);
		isAlive = 0;
	}

	public void activate(){
        whiteSprite.SetActive(true);
		isAlive = 1;
	}

    public void stage()
    {
        stagingSprite.SetActive(true);
    }

    public void unstage()
    {
        stagingSprite.SetActive(false);
    }



}
